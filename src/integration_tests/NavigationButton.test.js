import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import NavigationButton from '../components/NavigationButton';

describe('NavigationButton component', () => {
  const handlePageChange = jest.fn();

  test('renders the button with correct content and style', () => {
    render(
      <NavigationButton
        currentPage={1}
        breakpoint={2}
        handlePageChange={handlePageChange}
      >
        2
      </NavigationButton>
    );

    const navigationButton = screen.getByText('2');
    expect(navigationButton).toBeInTheDocument();
    expect(navigationButton).toHaveClass('hover:text-white hover:cursor-pointer hover:bg-primary-100');
  });

  test('calls handlePageChange function on button click', () => {
    render(
      <NavigationButton
        currentPage={1}
        breakpoint={2}
        handlePageChange={handlePageChange}
      >
        2
      </NavigationButton>
    );

    const navigationButton = screen.getByText('2');
    fireEvent.click(navigationButton);

    expect(handlePageChange).toHaveBeenCalledTimes(1);
    expect(handlePageChange).toHaveBeenCalledWith(2);
  });

  test('renders the button with the correct style for the current page', () => {
    render(
      <NavigationButton
        currentPage={2}
        breakpoint={2}
        handlePageChange={handlePageChange}
      >
        2
      </NavigationButton>
    );

    const navigationButton = screen.getByText('2');
    expect(navigationButton).toBeInTheDocument();
    expect(navigationButton).toHaveClass('bg-primary-whitegray select-none');
  });
});
