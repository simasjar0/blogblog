import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import { PostsContext } from "../context/PostsContext";
import { getDocs } from "firebase/firestore";
import Articles from "../pages/Articles";
import { MemoryRouter } from "react-router-dom";

class MockIntersectionObserver implements IntersectionObserver {
  readonly root: Element | null = null;
  readonly rootMargin: string = '';
  readonly thresholds: ReadonlyArray<number> = [];
  disconnect: () => void = () => {};
  observe: (target: Element) => void = () => {};
  takeRecords: () => IntersectionObserverEntry[] = () => [];
  unobserve: (target: Element) => void = () => {};

  constructor(callback: IntersectionObserverCallback, options?: IntersectionObserverInit) {}
}

declare global {
  namespace NodeJS {
    interface Global {
      IntersectionObserver: typeof MockIntersectionObserver;
    }
  }
}

global.IntersectionObserver = MockIntersectionObserver;

const originalIntersectionObserver = global.IntersectionObserver;

beforeAll(() => {
  // @ts-ignore
  global.IntersectionObserver = MockIntersectionObserver;
});

afterAll(() => {
  global.IntersectionObserver = originalIntersectionObserver;
});

jest.mock("firebase/firestore", () => ({
  ...jest.requireActual("firebase/firestore"),
  getDocs: jest.fn(),
}));

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useOutletContext: () => ({ loading: false, setLoading: jest.fn() }),
}));

const mockPosts: any = [
  {
    authorLabel: 'Author Label',
    imgUrl: 'https://images.unsplash.com/photo-1656444699089-bab3ba14aefc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2832&q=80',
    name: 'Mocked post title',
    pathname: '/article/mocked-post-article-path',
    writtenBy: 'Mocked User',
    writtenOn: new Date().getDate(),
  },
];

(getDocs as jest.Mock).mockImplementation(() => {
  return Promise.resolve({
    docs: mockPosts.map((post: any) => ({ data: () => post })),
  });
});

test("renders Articles component and its children components", async () => {
  const { container } = render(
    <MemoryRouter>
      <PostsContext.Provider value={{ posts: mockPosts, setPosts: jest.fn() }}>
        <Articles />
      </PostsContext.Provider>
    </MemoryRouter>
    
  );

  await waitFor(() => expect(screen.queryByText(/Loading/i)).not.toBeInTheDocument());

  mockPosts.forEach((post: any) => {
    expect(screen.getByText(post.name)).toBeInTheDocument();
  });

  const pagination = screen.getByTestId("pagination");
  expect(pagination).toBeInTheDocument();
});
