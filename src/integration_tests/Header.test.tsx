import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import Header from "../components/Header";

describe("Header component integration tests", () => {
  test("Renders Header and Navigation components and click on a NavLink", () => {
    render(
      <MemoryRouter initialEntries={["/"]}>
        <Header />
      </MemoryRouter>
    );

    const homeNavLink = screen.getByText("Home");
    const articlesNavLink = screen.getByText("Articles");
    const aboutNavLink = screen.getByText("About");
    const contactsNavLink = screen.getByText("Contacts");

    expect(homeNavLink).toBeInTheDocument();
    expect(articlesNavLink).toBeInTheDocument();
    expect(aboutNavLink).toBeInTheDocument();
    expect(contactsNavLink).toBeInTheDocument();

    fireEvent.click(articlesNavLink);

    expect(articlesNavLink).toHaveClass("text-color-active");
  });
});
