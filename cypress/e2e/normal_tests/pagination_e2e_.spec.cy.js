describe("Pagination Component", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/articles");
  });

  it("renders the pagination component with page numbers", () => {
    cy.get("[data-cy=pagination-component]").should("be.visible");
    cy.get("[data-cy=pagination-component] > div").children().should("have.length.above", 2);
  });

  it("navigates to the next page on click of the next button", () => {
    cy.get("[data-cy=pagination-component] .next-btn").click();
    cy.get("[data-cy=pagination-component] > div > div").first().should("have.text", "2");
  });

  it("navigates to the previous page on click of the previous button", () => {
    cy.get("[data-cy=pagination-component] .prev-btn").click();
    cy.get("[data-cy=pagination-component] > div > div").first().should("have.text", "1");
  });

  it("disables the next button when on the last page", () => {
    cy.get("[data-cy=pagination-component] .next-btn").click();
    cy.get("[data-cy=pagination-component] .next-btn").should("be.disabled");
  });

  it("disables the previous button when on the first page", () => {
    cy.get("[data-cy=pagination-component] .prev-btn").should("be.disabled");
  });

  it("navigates to the selected page on click of page number button", () => {
    cy.get("[data-cy=pagination-component] > div > div").eq(2).click();
    cy.get("[data-cy=pagination-component] > div > div").eq(2).should("have.class", "bg-primary-100 text-white");
  });
});