describe("Home Component", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/"); 
  });

  it("should render blog name, about topic, and blog intro", () => {
    cy.get(".blog-name").should("be.visible");
    cy.get(".leftside > header > span").should("be.visible");
    cy.get(".leftside > div > span").should("be.visible");
  });

  it("should render author image and author info", () => {
    cy.get(".rightside > .custom-image").should("be.visible");
    cy.get(".rightside > span.font-bold").should("be.visible");
    cy.get(".rightside > span.text-md").should("be.visible");
  });

  it("should render featured articles", () => {
    cy.get("#card-container")
      .find("CardContainer")
      .its("length")
      .should("be.gt", 0);
  });

  it("should navigate to articles page when 'View all articles' button is clicked", () => {
    cy.get("button")
      .contains("View all articles")
      .click();

    cy.url().should("include", "/articles");
  });

  it("should scroll to card-container when down arrow button is clicked", () => {
    cy.get(".w-16 > button").click();
    cy.wait(500);

    cy.get("#card-container").then(($cardContainer) => {
      const cardContainerTop = $cardContainer.offset().top;
      const windowHeight = Cypress.config("viewportHeight");
      expect(cardContainerTop).to.be.lessThanOrEqual(windowHeight);
    });
  });
});
