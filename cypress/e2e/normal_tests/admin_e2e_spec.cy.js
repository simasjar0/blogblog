describe('Admin Component E2E Test', () => {
  beforeEach(() => {
    cy.visit('localhost:3000/admin');
  });

  it('Login functionality works', () => {
    cy.get('input[name="email"]')
      .type('testuser@email.com') 
      .should('have.value', 'testuser@email.com');

    cy.get('input[name="password"]')
      .type('testpassword123')
      .should('have.value', 'testpassword123');

    cy.get('button[name="submit"]').click();

    cy.contains('Welcome, admin');
  });

  it('Select post dropdown works', () => {
    cy.get('input[name="email"]').type('testuser@email.com');
    cy.get('input[name="password"]').type('testpassword123');
    cy.get('button[name="submit"]').click();

    cy.get('.dropdown').click();
    cy.get('li').first().click();
    cy.get('.card-title').should('be.visible');
  });

});
