describe("NotFound Component", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/non-existing-route");
  });

  it("should render correctly", () => {
    cy.get(".text-7xl").contains("Oops...");
    cy.get(".text-5xl").contains("The page you are looking for is with our Lord now...");
    cy.get(".text-primary-500").contains("You will be redirected to Home page in");
  });

  it("should display the countdown and redirect to home after 10 seconds", () => {
    cy.get(".text-primary-700").contains("10");
    cy.wait(1000);
    cy.get(".text-primary-700").contains("9");
    cy.wait(1000);
    cy.get(".text-primary-700").contains("8");

    cy.wait(7000);
    cy.get(".text-primary-700").should("not.exist");

    cy.wait(750);
    cy.url().should("include", "/");
  });

  it("should display the spinner when the countdown reaches 0", () => {
    cy.wait(10000);
    cy.get(".text-primary-700").should("not.exist");
    cy.get("Spinner").should("be.visible");
  });
});